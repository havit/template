<?php


/** Variáveis de Configuração **/
return [
    'ambientes' =>
        [
            'producao' => [

                'caminho_base' => 'http://projeto.dev/'

            ],
            'desenvolvimento' => [

                'caminho_base' => 'http://projeto.dev/'
            ]
        ],

    'areas' => ['Site'],

    'padroes' => [
        'controller' => 'Site',
        'acao' => 'Principal',
        'area' => 'Site'
    ]
];






